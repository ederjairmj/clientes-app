import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './clientes/clientes.component';
import { DirectivaComponent } from './directiva/directiva.component';

const routes: Routes = [
  {path:'',redirectTo:'/clientes' ,pathMatch:'full'},
  {path:'clientes',component:ClientesComponent},
  {path:'directiva',component:DirectivaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
