import { Injectable } from '@angular/core';
import { Clientes } from './cliente.json';
import { Cliente } from './cliente';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor() { }

  getCliente(): Observable<Cliente[]> {
    return of(Clientes);
  }
}
